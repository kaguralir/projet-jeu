# ARCADE GAME 

> ✨ With the help of this tutorial: https://www.youtube.com/watch?v=g-FpDQ8Eqw8&
> > ✨ Created with Kaboom library
> > > ✨ Two levels 
> > > > ✨ Changing environement

***
## Concept
*![alt text](https://gitlab.com/kaguralir/projet-jeu/-/raw/master/public/assets/concept.jpg)


***
## Preview
*![alt text](https://gitlab.com/kaguralir/projet-jeu/-/raw/master/public/assets/preview2.png)

***
### ***Run***
Play here: https://kaguralir.gitlab.io/projet-jeu/

***

### Skills

* Basic NPM usage
* Collision
* ES6 Import/Export
* Using canvas and sprites
* Map 

***
### Info
Inspired by Mario bros

***
# **Enjoy!**


