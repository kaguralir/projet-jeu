export default class Compositor{ //draw all layers in order
    constructor(){
        this.layers=[]; //layer array
    }
    draw(context){
        this.layers.forEach(layer=>{//loops over the layers and draw the layers
            layer(context); //layer is a function that draw on context, needs all infor to draw itself
    });
}
}